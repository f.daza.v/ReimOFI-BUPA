package com.reim.chinoy.reimapp;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Bock on 21-01-2016.
 */
public class CursoActivo {

    private String idCurso;
    private String nomCurso;

    public CursoActivo(){

    }

    public String getIdCurso() {
        return idCurso;
    }

    public void setIdCurso(String idCurso) {
        this.idCurso = idCurso;
    }

    public String getNomCurso() {
        return nomCurso;
    }

    public void setNomCurso(String nomCurso) {
        this.nomCurso = nomCurso;
    }
}
