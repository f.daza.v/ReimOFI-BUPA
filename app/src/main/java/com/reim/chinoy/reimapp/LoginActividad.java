package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class LoginActividad extends AppCompatActivity {

    public ListView listaItems;
    SmartImageView avatarAlum;


    public String nomAlu;
    public String apAlu;
    public String idAlu;
    public String nomAluCom;

    public String[] nomAlumnos;
    public String[] apAlumnos;
    public String[] idAlumnos;
    public String[] nomAlumCompleto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_actividad);
        Usuarios usuario = (Usuarios) getApplicationContext();
        usuario.setvAlumno(0);
        asignarAlumnos();
        avatarAlum = (SmartImageView) findViewById(R.id.avatarAlumLogin);

        listaItems = (ListView) findViewById(R.id.listaAlumnos);
        listaItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Usuarios usuario = (Usuarios) getApplicationContext();
                usuario.setIdAlumno(idAlumnos[position]);
                usuario.setNomAlumno(nomAlumnos[position]);
                usuario.setApAlumno(apAlumnos[position]);

                TextView texto = (TextView) findViewById(R.id.nomAlumnoLogin);
                texto.setText(nomAlumCompleto[position]);
                Crouton.makeText(LoginActividad.this, "Escogio al Alumno: "+nomAlumCompleto[position], Style.ALERT).show();
                verAvatarAlum();
                usuario.setvAlumno(1);


            }
        });


    }

    public void verAvatarAlum() {
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/avatarUsuario/extraerAvatar.php?idUser=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        String img;
                        img = o.getString("avatar");
                        String url = "http://desarrolloreim.ulearnet.com/avatarUsuario/" + img + ".png";
                        Rect rect = new Rect(avatarAlum.getLeft(), avatarAlum.getTop(), avatarAlum.getRight(), avatarAlum.getBottom());
                        avatarAlum.setImageUrl(url, rect);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void ingresarReimLogin(View view){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idEdu = usuario.getIdEducadora();
        int alum = usuario.getvAlumno();
        if(alum == 1) {
            AsyncHttpClient client = new AsyncHttpClient();
            client.get("http://desarrolloreim.ulearnet.com/menuPrincipal/extraerPininfo.php?idEdu=" + idEdu, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                    if (statusCode == 200) {
                        try {
                            Usuarios usuario = (Usuarios) getApplicationContext();

                            JSONObject o = new JSONObject(new String(responseBody));
                            int reim = o.getInt("reim");
                            if (reim == 507) {
                                usuario.setvAlumno(0);
                                Intent intent = new Intent(LoginActividad.this, ReimBupa.class);
                                startActivity(intent);
                            }
                            if (reim == 508) {
                                usuario.setvAlumno(0);
                                Intent intent = new Intent(LoginActividad.this, ReimOfi.class);
                                startActivity(intent);
                            }
                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                }
            });
        }else{
            Crouton.makeText(LoginActividad.this, "Debe escoger un Alumno para ingresar a la actividad.", Style.ALERT).show();
        }
    }



    public void asignarAlumnos(){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idEdu = usuario.getIdEducadora();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/iniciarAct/obtenerAlum.php?idEdu="+idEdu, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {

                        JSONObject o = new JSONObject(new String(responseBody));
                        nomAlu = o.getString("nomAlumnos");
                        apAlu = o.getString("apAlumnos");
                        idAlu = o.getString("idAlumnos");
                        nomAluCom = o.getString("nombreCompleto");


                        nomAlumnos = nomAlu.split("[-]");
                        apAlumnos = apAlu.split("[-]");
                        idAlumnos = idAlu.split("[-]");
                        nomAlumCompleto = nomAluCom.split("[-]");



                        listaItems = (ListView) findViewById(R.id.listaAlumnos);

                        // Creamos el ArrayAdapter<String>
                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                getApplicationContext(), R.layout.list_black_text, R.id.list_content,
                                nomAlumCompleto);
                        // Establecemos el Adapter
                        listaItems.setAdapter(adapter);



                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }
}
