package com.reim.chinoy.reimapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Bock on 28/05/2016.
 */
public class BupaTrofeos extends AppCompatActivity {

    public SmartImageView avatarAlum;
    public MediaPlayer mediaPlayer;
    public MediaPlayer mediaPlayer2;
    public int segundos;
    public int cancion;
    public Timer timer;
    public Timer tCancion;
    public Context contexto;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bupa_trofeos);
        avatarAlum = (SmartImageView) findViewById(R.id.avatarAlumTrofeo);
        asignarAvatarAlumTrofeo();
        asignarTrofeos();

        segundos=0;
        cancion=0;
        mediaPlayer = MediaPlayer.create(this, R.raw.ambientetrofeo);
        mediaPlayer.start();
        Contar2();
        contexto = getApplicationContext();

    }

    //FUNCION PARA VOLVER AL MENU
    public void volverMenuTrofeos(View v){
        mediaPlayer.release();
        System.gc();
        finish();
        Intent intent = new Intent(BupaTrofeos.this, ReimBupa.class);
        startActivity(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    //ASIGNA AVATAR DEL USUARIO
    public void asignarAvatarAlumTrofeo(){

        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/avatarUsuario/extraerAvatar.php?idUser=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        String img;
                        img = o.getString("avatar");
                        String url = "http://desarrolloreim.ulearnet.com/avatarUsuario/" + img + "2.png";
                        Rect rect = new Rect(avatarAlum.getLeft(), avatarAlum.getTop(), avatarAlum.getRight(), avatarAlum.getBottom());
                        avatarAlum.setImageUrl(url, rect);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }



    public void obtenerTrofeo1(View v){

        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/trofeos/canjearTrofeos.php?idAlum="+idAlum+"&idTrofeo=1&cantEst=10", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        int res = o.getInt("respuesta");
                        int can = o.getInt("canjeo");
                        String canjeo = o.getString("canjeo");

                        if(can == 999){
                            TextView cantidad = (TextView) findViewById(R.id.cantMonedas);
                            cantidad.setText("0");
                        }else{
                            TextView cantidad = (TextView) findViewById(R.id.cantMonedas);
                            cantidad.setText(canjeo);
                        }

                        if(res == 1){
                            mediaPlayer.pause();
                            mediaPlayer2 = MediaPlayer.create(contexto, R.raw.canjeo);
                            mediaPlayer2.start();
                            segundos = 0;
                            Contar();
                            Toast.makeText(getApplicationContext(), "Canjeo realizado.", Toast.LENGTH_LONG).show();
                            ImageView img1 = (ImageView) findViewById(R.id.imagenTrofeo1);
                            ImageView imgM1 = (ImageView) findViewById(R.id.monedaTrofeo1);
                            TextView tmp1 = (TextView) findViewById(R.id.marcoPrecio1);
                            TextView tmc1 = (TextView) findViewById(R.id.marcoCosto1);
                            TextView tc1 = (TextView) findViewById(R.id.cantNumero1);

                            img1.setImageResource(R.drawable.trofeo0);
                            img1.setClickable(false);
                            imgM1.setVisibility(View.INVISIBLE);
                            tmp1.setVisibility(View.INVISIBLE);
                            tmc1.setVisibility(View.INVISIBLE);
                            tc1.setVisibility(View.INVISIBLE);
                        }else{
                            Toast.makeText(getApplicationContext(), "No tienes monedas suficientes.", Toast.LENGTH_LONG).show();
                        }



                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }
    public void obtenerTrofeo2(View v){

        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/trofeos/canjearTrofeos.php?idAlum=" + idAlum + "&idTrofeo=2&cantEst=20", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        int res = o.getInt("respuesta");
                        int can = o.getInt("canjeo");
                        String canjeo = o.getString("canjeo");

                        if (can == 999) {
                            TextView cantidad = (TextView) findViewById(R.id.cantMonedas);
                            cantidad.setText("0");
                        } else {
                            TextView cantidad = (TextView) findViewById(R.id.cantMonedas);
                            cantidad.setText(canjeo);
                        }

                        if (res == 1) {
                            mediaPlayer.pause();
                            mediaPlayer2 = MediaPlayer.create(contexto, R.raw.canjeo);
                            mediaPlayer2.start();
                            segundos = 0;
                            Contar();
                            Toast.makeText(getApplicationContext(), "Canjeo realizado.", Toast.LENGTH_LONG).show();
                            ImageView img1 = (ImageView) findViewById(R.id.imagenTrofeo2);
                            ImageView imgM1 = (ImageView) findViewById(R.id.monedaTrofeo2);
                            TextView tmp1 = (TextView) findViewById(R.id.marcoPrecio2);
                            TextView tmc1 = (TextView) findViewById(R.id.marcoCosto2);
                            TextView tc1 = (TextView) findViewById(R.id.cantNumero2);

                            img1.setImageResource(R.drawable.trofeo1);
                            img1.setClickable(false);
                            imgM1.setVisibility(View.INVISIBLE);
                            tmp1.setVisibility(View.INVISIBLE);
                            tmc1.setVisibility(View.INVISIBLE);
                            tc1.setVisibility(View.INVISIBLE);
                        } else {
                            Toast.makeText(getApplicationContext(), "No tienes monedas suficientes.", Toast.LENGTH_LONG).show();
                        }


                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }
    public void obtenerTrofeo3(View v){

        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/trofeos/canjearTrofeos.php?idAlum=" + idAlum + "&idTrofeo=3&cantEst=30", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        int res = o.getInt("respuesta");
                        int can = o.getInt("canjeo");
                        String canjeo = o.getString("canjeo");

                        if (can == 999) {
                            TextView cantidad = (TextView) findViewById(R.id.cantMonedas);
                            cantidad.setText("0");
                        } else {
                            TextView cantidad = (TextView) findViewById(R.id.cantMonedas);
                            cantidad.setText(canjeo);
                        }

                        if (res == 1) {
                            mediaPlayer.pause();
                            mediaPlayer2 = MediaPlayer.create(contexto, R.raw.canjeo);
                            mediaPlayer2.start();
                            segundos = 0;
                            Contar();
                            Toast.makeText(getApplicationContext(), "Canjeo realizado.", Toast.LENGTH_LONG).show();
                            ImageView img1 = (ImageView) findViewById(R.id.imagenTrofeo3);
                            ImageView imgM1 = (ImageView) findViewById(R.id.monedaTrofeo3);
                            TextView tmp1 = (TextView) findViewById(R.id.marcoPrecio3);
                            TextView tmc1 = (TextView) findViewById(R.id.marcoCosto3);
                            TextView tc1 = (TextView) findViewById(R.id.cantNumero3);

                            img1.setImageResource(R.drawable.trofeo2);
                            img1.setClickable(false);
                            imgM1.setVisibility(View.INVISIBLE);
                            tmp1.setVisibility(View.INVISIBLE);
                            tmc1.setVisibility(View.INVISIBLE);
                            tc1.setVisibility(View.INVISIBLE);
                        } else {
                            Toast.makeText(getApplicationContext(), "No tienes monedas suficientes.", Toast.LENGTH_LONG).show();
                        }


                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void asignarTrofeos(){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/trofeos/extraerTrofeos.php?idAlum=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        int t1 = o.getInt("trofeo1");
                        int t2 = o.getInt("trofeo2");
                        int t3 = o.getInt("trofeo3");
                        String monedas = o.getString("est");
                        int mon = o.getInt("est");

                        if(mon == 999){
                            TextView cantidad = (TextView) findViewById(R.id.cantMonedas);
                            cantidad.setText("0");
                        }else{
                            TextView cantidad = (TextView) findViewById(R.id.cantMonedas);
                            cantidad.setText(monedas);
                        }


                        if(t1 == 2){
                            ImageView img1 = (ImageView) findViewById(R.id.imagenTrofeo1);
                            img1.setImageResource(R.drawable.trofeo0);
                            img1.setClickable(false);

                        }else{
                            ImageView img1 = (ImageView) findViewById(R.id.imagenTrofeo1);
                            ImageView imgM1 = (ImageView) findViewById(R.id.monedaTrofeo1);
                            TextView tmp1 = (TextView) findViewById(R.id.marcoPrecio1);
                            TextView tmc1 = (TextView) findViewById(R.id.marcoCosto1);
                            TextView tc1 = (TextView) findViewById(R.id.cantNumero1);

                            img1.setImageResource(R.drawable.trofeo02);
                            imgM1.setVisibility(View.VISIBLE);
                            tmp1.setVisibility(View.VISIBLE);
                            tmc1.setVisibility(View.VISIBLE);
                            tc1.setVisibility(View.VISIBLE);
                        }

                        if(t2 == 2){
                            ImageView img1 = (ImageView) findViewById(R.id.imagenTrofeo2);
                            img1.setImageResource(R.drawable.trofeo1);
                            img1.setClickable(false);

                        }else{
                            ImageView img1 = (ImageView) findViewById(R.id.imagenTrofeo2);
                            ImageView imgM1 = (ImageView) findViewById(R.id.monedaTrofeo2);
                            TextView tmp1 = (TextView) findViewById(R.id.marcoPrecio2);
                            TextView tmc1 = (TextView) findViewById(R.id.marcoCosto2);
                            TextView tc1 = (TextView) findViewById(R.id.cantNumero2);

                            img1.setImageResource(R.drawable.trofeo12);
                            imgM1.setVisibility(View.VISIBLE);
                            tmp1.setVisibility(View.VISIBLE);
                            tmc1.setVisibility(View.VISIBLE);
                            tc1.setVisibility(View.VISIBLE);
                        }

                        if(t3 == 2){
                            ImageView img1 = (ImageView) findViewById(R.id.imagenTrofeo3);
                            img1.setImageResource(R.drawable.trofeo2);
                            img1.setClickable(false);

                        }else{
                            ImageView img1 = (ImageView) findViewById(R.id.imagenTrofeo3);
                            ImageView imgM1 = (ImageView) findViewById(R.id.monedaTrofeo3);
                            TextView tmp1 = (TextView) findViewById(R.id.marcoPrecio3);
                            TextView tmc1 = (TextView) findViewById(R.id.marcoCosto3);
                            TextView tc1 = (TextView) findViewById(R.id.cantNumero3);

                            img1.setImageResource(R.drawable.trofeo22);
                            imgM1.setVisibility(View.VISIBLE);
                            tmp1.setVisibility(View.VISIBLE);
                            tmc1.setVisibility(View.VISIBLE);
                            tc1.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }



    //Clase interna que funciona como contador

    class Cancion extends TimerTask {
        public void run() {
            segundos++;
            if(segundos == 9){
                mediaPlayer2.stop();
                mediaPlayer.start();
                Detener();
            }

            //System.out.println("segundo: " + segundos);
        }
    }
    class Cancion2 extends TimerTask {
        public void run() {
            cancion++;
            if(cancion == 120){
                Detener2();
                mediaPlayer.stop();
                mediaPlayer.start();
                cancion = 0;
                Contar2();
            }
        }
    }

    //Crea un timer, inicia segundos a 0 y comienza a contar

    public void Contar()
    {
        this.segundos=0;
        timer = new Timer();
        timer.schedule(new Cancion(), 0, 1000);
    }
    //Detiene el contador
    public void Detener() {
        timer.cancel();
    }


    public void Contar2()
    {
        this.cancion=0;
        tCancion = new Timer();
        tCancion.schedule(new Cancion2(), 0, 1000);
    }
    //Detiene el contador
    public void Detener2() {
        tCancion.cancel();
    }
}
