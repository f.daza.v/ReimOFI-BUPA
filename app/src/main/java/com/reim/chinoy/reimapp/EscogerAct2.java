package com.reim.chinoy.reimapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by Bock on 31/05/2016.
 */
public class EscogerAct2 extends AppCompatActivity {


    public Context contexto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escoger2);
        asignarConfirmacion();
        Usuarios usuario = (Usuarios) getApplicationContext();
        usuario.setvConfirmacion(0);
        contexto = getApplicationContext();
    }

    public void ingActividad(View v) {//escoger actividad

        final Usuarios usuario = (Usuarios) getApplicationContext();
        String idEdu = usuario.getIdEducadora();
        String curso = usuario.getCursoActivo();
        String reim = usuario.getReimActivo();


        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/escogerReim/ingresarAct.php?idEdu=" + idEdu + "&idReim=" + reim + "&idCur=" + curso, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        int res = o.getInt("respuesta");
                        String pin = o.getString("pin");

                        if (res == 1) {
                            //Toast.makeText(getApplicationContext(), "Ingreso correctamente la actividad.", Toast.LENGTH_LONG).show();
                            Crouton.makeText(EscogerAct2.this, "Ingreso correctamente la actividad.", Style.ALERT).show();
                            TextView t1 = (TextView) findViewById(R.id.pinRecibido);
                            TextView t2 = (TextView) findViewById(R.id.nomPin);
                            t1.setText(pin);
                            t1.setVisibility(View.VISIBLE);
                            t2.setVisibility(View.VISIBLE);

                            Button b1 = (Button) findViewById(R.id.volverMenuCon);
                            b1.setVisibility(View.VISIBLE);
                            usuario.setvConfirmacion(1);

                        }


                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            Usuarios usuario = (Usuarios) getApplicationContext();
            int conf = usuario.getvConfirmacion();
            if(conf == 1){
                usuario.setvConfirmacion(0);
                Intent intent = new Intent(EscogerAct2.this, MenuPrin.class);
                startActivity(intent);
            }else{
                usuario.setvConfirmacion(0);
                Intent intent = new Intent(EscogerAct2.this, EscogerAct.class);
                startActivity(intent);
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    public void volverMCon(View v){
        Intent intent = new Intent(EscogerAct2.this, MenuPrin.class);
        startActivity(intent);
    }

    public void asignarConfirmacion(){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String curso = usuario.getNomCursoActivo();
        String reim = usuario.getReimActivo();
        int idReim=Integer.parseInt(reim.trim());

        if(idReim == 507){
            ImageView img = (ImageView) findViewById(R.id.reimConfirmar);
            TextView texto = (TextView) findViewById(R.id.nomReimCon);
            img.setImageResource(R.drawable.bupacon);
            texto.setText("BUPA");

        }
        if(idReim == 508){
            ImageView img = (ImageView) findViewById(R.id.reimConfirmar);
            TextView texto = (TextView) findViewById(R.id.nomReimCon);
            img.setImageResource(R.drawable.oficon);
            texto.setText("OFI");
        }

        TextView nomC = (TextView) findViewById(R.id.cursoConfirmar);
        nomC.setText(curso);


    }
}
