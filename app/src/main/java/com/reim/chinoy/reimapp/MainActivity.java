package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.preference.PreferenceActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import org.json.JSONException;
import org.json.JSONObject;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void cerrarAplicacion(View v){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void prueba(View v){
        Intent intent = new Intent(MainActivity.this, Prueba.class);
        startActivity(intent);
    }

    public void ValidarOnClick(View v){
        EditText et_login;
        EditText et_password;

        et_login = (EditText) findViewById(R.id.et_login);
        et_password = (EditText) findViewById(R.id.et_password);

        AsyncHttpClient client = new AsyncHttpClient();
        String url = "http://desarrolloreim.ulearnet.com/loginReim/login2.php";

        //Reuno datos para hacer el post
        RequestParams requestParams = new RequestParams();
        requestParams.add("login",et_login.getText().toString());
        requestParams.add("password",et_password.getText().toString());


        //realizo el post en el php
        RequestHandle post = client.post(url, requestParams, new AsyncHttpResponseHandler() {
            String idEducadora=null;
            String nomEducadora=null;
            String apeEducadora=null;

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(statusCode==200){
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        idEducadora = o.getString("userId");
                        nomEducadora = o.getString("userNombre");
                        apeEducadora = o.getString("userApellido");


                        if(!TextUtils.isEmpty(idEducadora)){

                            Usuarios usuario = (Usuarios) getApplicationContext();
                            usuario.setIdEducadora(idEducadora);
                            usuario.setNomEducadora(nomEducadora);
                            usuario.setApeEducadora(apeEducadora);

                            Intent intent = new Intent(MainActivity.this, MenuPrin.class);
                            startActivity(intent);
                        }else{
                            Crouton.makeText(MainActivity.this,"Error de ingreso", Style.ALERT).show();
                        }
                    }catch (JSONException e) {  }

                }else{
                    Crouton.makeText(MainActivity.this,"Error de conexion al servidor.", Style.ALERT).show();
                }
            }
            // ...
            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable throwable) {

            }

        });

    }
}
