package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SituacionConstructor extends AppCompatActivity implements View.OnClickListener{

    Tiempo tiempo=new Tiempo();
    SmartImageView avatarAlum;
    java.util.Date utilDate = new java.util.Date();
    java.sql.Date  sqlDate  = new java.sql.Date(utilDate.getTime());

    MediaPlayer loquebuscaba   = new MediaPlayer();
    MediaPlayer noesloquebusco = new MediaPlayer();
    MediaPlayer ayudame=new MediaPlayer();


    ImageView situacionct;
    ImageButton alt1sct;
    ImageButton alt2sct;
    ImageButton alt3sct;
    ImageButton BtnAudio;
    ImageButton menu;

    int idambiente=2;
    int item=1;
    int idalternativa=1;
    int toques=1;
    int correcta=0;
    int time=0;



    String query2="INSERT INTO resultados_reim (id_resultado, id_alumno, id_instrumento, id_actividad,id_item, id_alternativa, c_touch,c_audio,tiempo,fecha, hora, es_correcta)"+
            "VALUES(null,?,508,4,?,?,?,?,?,?,?,?)";

    /*String query="INSERT INTO resultado_reim_ofi (id_resultado, id_alumno, id_instrumento, id_actividad,id_ambiente, id_item, id_alternativa, c_touch,tiempo,fecha, hora, es_correcta, imagen)"+
            "VALUES(null,?,508,6,?,?,?,?,?,?,?,?,?)";*/ //1 id alumno 2 id_ambiente 3 id item 4 id alternativa 5 ctouch 6 tiempo 7 fecha, 8 hora,9 es correcta, 10 imagen

    String imagen="";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_situacion_constructor);

        ayudame=MediaPlayer.create(SituacionConstructor.this,R.raw.ayudame_a_encontrar_lo_que_necesito);
        noesloquebusco=MediaPlayer.create(SituacionConstructor.this,R.raw.no_es_lo_que_busco);
        loquebuscaba=MediaPlayer.create(SituacionConstructor.this,R.raw.justo_lo_que_buscaba);


        situacionct = (ImageView) findViewById(R.id.situacionconstru);
        alt1sct = (ImageButton) findViewById(R.id.alt1sct);
        alt2sct = (ImageButton) findViewById(R.id.alt2sct);
        alt3sct = (ImageButton) findViewById(R.id.alt3sct);
        BtnAudio= (ImageButton) findViewById(R.id.AudioSct);
        menu=(ImageButton)findViewById(R.id.Sitctmenu);

        avatarAlum     = (SmartImageView) findViewById(R.id.avatarAlumSct);
        asignarAvatarAlumMB();

        tiempo.Contar();


        alt1sct.setOnClickListener(this);
        alt2sct.setOnClickListener(this);
        alt3sct.setOnClickListener(this);
        menu.setOnClickListener(this);
        BtnAudio.setOnClickListener(this);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.alt1sct:
                idalternativa=1;
                if (item==1){

                    imagen="carretilla";
                    time=tiempo.getSegundos();
                    consulta qalt1sb = new consulta();
                    qalt1sb.execute();
                    correcta=2;
                    loquebuscaba.start();

                    alt1sct.setBackgroundResource(R.drawable.guitarra);
                    alt2sct.setBackgroundResource(R.drawable.atornillador);
                    alt3sct.setBackgroundResource(R.drawable.paraguas);
                    situacionct.setBackgroundResource(R.drawable.atornillar);
                    item++;


                }else{
                    switch (item){

                        case 2:
                            imagen="guitarra";
                            time=tiempo.getSegundos();
                            correcta=1;
                            noesloquebusco.start();
                            consulta a1 = new consulta();
                            a1.execute();
                            break;
                        case 3:
                            imagen="guitarra";
                            correcta=1;
                            noesloquebusco.start();
                            time=tiempo.getSegundos();
                            consulta a2 = new consulta();
                            a2.execute();
                            break;


                    }


                }


                break;

            case R.id.alt2sct:
                idalternativa=2;
                if (item==2){

                    imagen="atornillador";
                    time=tiempo.getSegundos();
                    consulta qalt1sb = new consulta();
                    qalt1sb.execute();
                    correcta=1;
                    loquebuscaba.start();

                    alt1sct.setBackgroundResource(R.drawable.guitarra);
                    alt2sct.setBackgroundResource(R.drawable.juguera);
                    alt3sct.setBackgroundResource(R.drawable.ladrillo);
                    situacionct.setBackgroundResource(R.drawable.muralla);
                    item++;






                }else{
                    switch (item){

                        case 1:
                            imagen="paraguas";
                            time=tiempo.getSegundos();
                            correcta=1;
                            noesloquebusco.start();
                            consulta a1 = new consulta();
                            a1.execute();
                            break;
                        case 3:
                            imagen="juguera";
                            correcta=1;
                            noesloquebusco.start();
                            time=tiempo.getSegundos();
                            consulta a2 = new consulta();
                            a2.execute();
                            break;


                    }

                }


                break;

            case R.id.alt3sct:
                idalternativa=3;
                if (item==3){

                    imagen="ladrillo";
                    time=tiempo.getSegundos();
                    consulta qalt1sb = new consulta();
                    qalt1sb.execute();
                    correcta=2;
                    loquebuscaba.start();

                    tiempo.Detener();

                    Intent intent = new Intent(SituacionConstructor.this,EntregaListon3_OFI.class);
                    startActivity(intent);

                }else{
                    switch (item){

                        case 1:
                            imagen="lampara";
                            time=tiempo.getSegundos();
                            correcta=1;
                            noesloquebusco.start();
                            consulta a1 = new consulta();
                            a1.execute();
                            break;
                        case 2:
                            imagen="paraguas";
                            correcta=1;
                            noesloquebusco.start();
                            time=tiempo.getSegundos();
                            consulta a2 = new consulta();
                            a2.execute();
                            break;


                    }

                }


                break;


            case R.id.AudioSct:
                ayudame.start();
                break;

            case R.id.Sitctmenu:
                Intent intentmenu = new Intent(SituacionConstructor.this,ReimOfi.class);
                startActivity(intentmenu);



        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void asignarAvatarAlumMB(){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/avatarUsuario/extraerAvatar.php?idUser=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        String img;
                        img = o.getString("avatar");
                        String url = "http://desarrolloreim.ulearnet.com/avatarUsuario/" + img + ".png";
                        Rect rect = new Rect(avatarAlum.getLeft(), avatarAlum.getTop(), avatarAlum.getRight(), avatarAlum.getBottom());
                        avatarAlum.setImageUrl(url, rect);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }



    private class consulta extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            alt1sct.setEnabled(false);
            alt2sct.setEnabled(false);
            alt3sct.setEnabled(false);
            BtnAudio.setEnabled(false);


        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            alt1sct.setEnabled(true);
            alt2sct.setEnabled(true);
            alt3sct.setEnabled(true);
            BtnAudio.setEnabled(true);

        }

        @Override
        protected Void doInBackground(Void... params) {
            Usuarios usuario = (Usuarios) getApplicationContext();
            int  idusuario= Integer.parseInt(usuario.getIdAlumno());

            try {
                Date utilhora = new Date();

                String mydate =java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                String [] fechahora=mydate.split(" ");

                SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                utilhora = formatter.parse(fechahora[1]);
                java.sql.Time sqlhora = new java.sql.Time(utilhora.getTime());

                Class.forName("com.mysql.jdbc.Driver");
                String url = "jdbc:mysql://mysql.ulearnet.com:3306/ulearnet_reim";//"jdbc:mysql:///10.0.3.2:3306/dbname"
                Connection c = DriverManager.getConnection(url, "fdaza", "felipe2016");//password_you_wish;
                PreparedStatement st = c.prepareStatement(query2);
                //1 id alumno 2 id_ambiente 3 id item 4 id alternativa 5 ctouch 6 tiempo 7 fecha, 8 hora,9 es correcta, 10 imagen

                st.setInt(1, idusuario);//22712
                st.setInt(2,item);
                st.setInt(3,idalternativa);
                st.setInt(4,toques);
                st.setInt(5,1);
                st.setInt(6, time);
                st.setDate(7, sqlDate);
                st.setTime(8, sqlhora);
                st.setInt(9, correcta);

                st.execute();
                st.close();
                c.close();
            }
            catch (ClassNotFoundException | SQLException | ParseException e){ //SQLException e
                e.printStackTrace();
            }




            // ResultadoReim(query);



            return null;
        }
    }



}
