package com.reim.chinoy.reimapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

/**
 * Created by Bock on 06/06/2016.
 */
public class Resultados extends AppCompatActivity {
    private WebView browser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_resultados);
        browser = (WebView)findViewById(R.id.webkit);
        //habilitamos javascript y el zoom
        browser.getSettings().setJavaScriptEnabled(true);
        browser.getSettings().setBuiltInZoomControls(true);
        browser.loadUrl("http://desarrolloreim.ulearnet.com/plataformaReim/");
    }
}
