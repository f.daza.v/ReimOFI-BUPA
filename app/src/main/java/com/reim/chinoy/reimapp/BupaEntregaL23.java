package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;

/**
 * Created by Bock on 27/05/2016.
 */
public class BupaEntregaL23 extends AppCompatActivity {
    public  MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bupa_liston23);

        mediaPlayer = MediaPlayer.create(this, R.raw.sonidomedalla);
        mediaPlayer.start();

    }

    public void bupaEntregaL23(View view){
        mediaPlayer.release();
        System.gc();
        finish();
        Intent intent = new Intent(BupaEntregaL23.this, BupaAct33.class);
        startActivity(intent);
    }

    //FUNCION QUE EVITAR QUE SE PUEDA RETROCEDER CON BOTON VOLVER
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
