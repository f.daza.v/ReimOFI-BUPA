package com.reim.chinoy.reimapp;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;

/**
 * Created by Bock on 22-01-2016.
 */
public class ReimAct extends AppCompatActivity{
    private String idReim;
    private String nomReim;


    public ReimAct(){

    }

    public String getIdReim() {
        return idReim;
    }

    public void setIdReim(String idReim) {
        this.idReim = idReim;
    }

    public String getNomReim() {
        return nomReim;
    }

    public void setNomReim(String nomReim) {
        this.nomReim = nomReim;
    }

    public void setBackgroundReim(String ambiente){

            FrameLayout layout =(FrameLayout)findViewById(R.id.bupa_act1);
            layout.setBackgroundResource(R.drawable.mercado);
            Toast.makeText(getApplicationContext(), "el ambiente actual es: " + ambiente, Toast.LENGTH_LONG).show();


    }


}
