package com.reim.chinoy.reimapp;

import android.app.Application;

/**
 * Created by Chinoy on 26-11-2015.
 */
public class Educadora extends Application {

    private String idEducadora;
    private String nomEducadora;
    private String apeEducadora;

    public void setApeEducadora(String apeEducadora) {
        this.apeEducadora = apeEducadora;
    }

    public void setIdEducadora(String idEducadora) {
        this.idEducadora = idEducadora;
    }

    public void setNomEducadora(String nomEducadora) {
        this.nomEducadora = nomEducadora;
    }

    public String getApeEducadora() {
        return apeEducadora;
    }

    public String getIdEducadora() {
        return idEducadora;
    }

    public String getNomEducadora() {
        return nomEducadora;
    }
}
