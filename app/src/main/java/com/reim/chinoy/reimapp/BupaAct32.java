package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Bock on 27/05/2016.
 */
public class BupaAct32 extends AppCompatActivity {

    public SmartImageView avatarAlum;
    public MediaPlayer respuesta;
    public int cantAudio = 0;
    public String idAlternativas;
    public String[] idAlt;
    public String nomAlternativas;
    public String[] nomAlt;
    public String nomAudioBD;
    public String itemActual;

    public Timer timer = new Timer();
    public int segundos=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bupa_actividad32);

        avatarAlum = (SmartImageView) findViewById(R.id.avatarAlumRA32);
        Contar();
        asignarAvatarAlumA32();
        asignarImagenesAct32();
    }

    //ASIGNA AVATAR DEL USUARIO
    public void asignarAvatarAlumA32(){

        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/avatarUsuario/extraerAvatar.php?idUser=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        String img;
                        img = o.getString("avatar");
                        String url = "http://desarrolloreim.ulearnet.com/avatarUsuario/" + img + "2.png";
                        Rect rect = new Rect(avatarAlum.getLeft(), avatarAlum.getTop(), avatarAlum.getRight(), avatarAlum.getBottom());
                        avatarAlum.setImageUrl(url, rect);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    //FUNCION PARA EL BOTON DE AUDIO, REPRODUCE EL SONIDO DE LA PREGUNTA ACTUAL
    public void audioBupaAct32(View v){
        TextView txtView =(TextView) findViewById(R.id.nomAudioActualA32);
        String fname=txtView.getText().toString().toLowerCase();
        int resID=getResources().getIdentifier(fname, "raw", getPackageName());
        MediaPlayer mediaPlayer = MediaPlayer.create(this, resID);
        mediaPlayer.start();
        cantAudio = cantAudio+1;
    }

    //FUNCION QUE EVITAR QUE SE PUEDA RETROCEDER CON BOTON VOLVER
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    //FUNCION PARA SALIR DEL REIM
    public void salirBupaA32(View v){

        Intent intent = new Intent(BupaAct32.this, Temporal.class);
        startActivity(intent);
    }

    //FUNCION PARA VOLVER AL MENU
    public void volverMenuBA32(View v){
        Intent intent = new Intent(BupaAct32.this, ReimBupa.class);
        startActivity(intent);
    }

    //FUNCION QUE ENVIA ALUMNO AL SERVIDOR Y RECIBE LAS ALTERNATIVAS.
    public void asignarImagenesAct32(){
        final Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/actividad3/bupaAct3.php?idAlum=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (statusCode == 200) {
                    try {
                        int amb;
                        JSONObject o = new JSONObject(new String(responseBody));
                        idAlternativas = o.getString("idAlternativas");
                        nomAlternativas = o.getString("nomAlternativas");
                        nomAudioBD = o.getString("audioBupa");
                        itemActual = o.getString("itemActual");
                        amb = o.getInt("ambienteActual");

                        TextView texto = (TextView) findViewById(R.id.nomAudioActualA32);
                        texto.setText(o.getString("audioBupa"));

                        nomAlt = nomAlternativas.split("[-]");
                        idAlt = idAlternativas.split("[-]");

                        String recurso = "drawable";

                        ImageView alt1 = (ImageView) findViewById(R.id.alternativa1A32);
                        ImageView alt2 = (ImageView) findViewById(R.id.alternativa2A32);
                        ImageView alt3 = (ImageView) findViewById(R.id.alternativa3A32);
                        ImageView alt4 = (ImageView) findViewById(R.id.alternativa4A32);

                        String img1 = nomAlt[0];
                        String img2 = nomAlt[1];
                        String img3 = nomAlt[2];
                        String img4 = nomAlt[3];

                        int res_imagen1 = getResources().getIdentifier(img1, recurso, getPackageName());
                        alt1.setImageResource(res_imagen1);
                        int res_imagen2 = getResources().getIdentifier(img2, recurso, getPackageName());
                        alt2.setImageResource(res_imagen2);
                        int res_imagen3 = getResources().getIdentifier(img3, recurso, getPackageName());
                        alt3.setImageResource(res_imagen3);
                        int res_imagen4 = getResources().getIdentifier(img4, recurso, getPackageName());
                        alt4.setImageResource(res_imagen4);


                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    //RESPUESTA DE LA ALTERNATIVA 1
    public void alternativa1Act32(View v){

        Usuarios usuario = (Usuarios) getApplicationContext();
        final String idAlum = usuario.getIdAlumno();

        Detener();
        respuesta = MediaPlayer.create(getApplicationContext(), R.raw.rescorrecta);
        respuesta.start();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/actividad3/resBupaAct3.php?idAlum=" + idAlum + "&idItem=" + itemActual + "&idAlt=" + idAlt[0] + "&cAudio=" + cantAudio+"&cTiempo="+segundos, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        int lis2;
                        int res;
                        JSONObject o = new JSONObject(new String(responseBody));
                        lis2 = o.getInt("liston2");
                        res = o.getInt("correcta");


                        if (lis2 == 2) {
                            respuesta.release();
                            System.gc();
                            finish();
                            Intent intent = new Intent(BupaAct32.this, BupaEntregaL23.class);
                            startActivity(intent);
                        } else {
                            asignarImagenesAct32();
                            segundos = 0;
                            cantAudio = 0;
                            Contar();
                            //Intent intent = new Intent(BupaAct32.this, BupaAct32.class);
                            //startActivity(intent);
                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    //RESPUESTA DE LA ALTERNATIVA 2
    public void alternativa2Act32(View v){

        Usuarios usuario = (Usuarios) getApplicationContext();
        final String idAlum = usuario.getIdAlumno();

        Detener();
        respuesta = MediaPlayer.create(getApplicationContext(), R.raw.rescorrecta);
        respuesta.start();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/actividad3/resBupaAct3.php?idAlum=" + idAlum + "&idItem=" + itemActual + "&idAlt=" + idAlt[1] + "&cAudio=" + cantAudio+"&cTiempo="+segundos, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        int lis2;
                        int res;
                        JSONObject o = new JSONObject(new String(responseBody));
                        lis2 = o.getInt("liston2");
                        res = o.getInt("correcta");


                        if (lis2 == 2) {
                            respuesta.release();
                            System.gc();
                            finish();
                            Intent intent = new Intent(BupaAct32.this, BupaEntregaL23.class);
                            startActivity(intent);
                        } else {
                            asignarImagenesAct32();
                            segundos = 0;
                            cantAudio = 0;
                            Contar();
                            //Intent intent = new Intent(BupaAct32.this, BupaAct32.class);
                            //startActivity(intent);
                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    //RESPUESTA DE LA ALTERNATIVA 3
    public void alternativa3Act32(View v){

        Usuarios usuario = (Usuarios) getApplicationContext();
        final String idAlum = usuario.getIdAlumno();

        Detener();
        respuesta = MediaPlayer.create(getApplicationContext(), R.raw.rescorrecta);
        respuesta.start();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/actividad3/resBupaAct3.php?idAlum=" + idAlum + "&idItem=" + itemActual + "&idAlt=" + idAlt[2] + "&cAudio=" + cantAudio+"&cTiempo="+segundos, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        int lis2;
                        int res;
                        JSONObject o = new JSONObject(new String(responseBody));
                        lis2 = o.getInt("liston2");
                        res = o.getInt("correcta");


                        if (lis2 == 2) {
                            respuesta.release();
                            System.gc();
                            finish();
                            Intent intent = new Intent(BupaAct32.this, BupaEntregaL23.class);
                            startActivity(intent);
                        } else {
                            asignarImagenesAct32();
                            segundos = 0;
                            cantAudio = 0;
                            Contar();
                            //Intent intent = new Intent(BupaAct32.this, BupaAct32.class);
                            //startActivity(intent);
                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    //RESPUESTA DE LA ALTERNATIVA 4
    public void alternativa4Act32(View v){

        Usuarios usuario = (Usuarios) getApplicationContext();
        final String idAlum = usuario.getIdAlumno();

        Detener();
        respuesta = MediaPlayer.create(getApplicationContext(), R.raw.rescorrecta);
        respuesta.start();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/actividad3/resBupaAct3.php?idAlum=" + idAlum + "&idItem=" + itemActual + "&idAlt=" + idAlt[3] + "&cAudio=" + cantAudio+"&cTiempo="+segundos, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        int lis2;
                        int res;
                        JSONObject o = new JSONObject(new String(responseBody));
                        lis2 = o.getInt("liston2");
                        res = o.getInt("correcta");


                        if (lis2 == 2) {
                            respuesta.release();
                            System.gc();
                            finish();
                            Intent intent = new Intent(BupaAct32.this, BupaEntregaL23.class);
                            startActivity(intent);
                        } else {
                            asignarImagenesAct32();
                            segundos = 0;
                            cantAudio = 0;
                            Contar();
                            //Intent intent = new Intent(BupaAct32.this, BupaAct32.class);
                            //startActivity(intent);
                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    //Clase interna que funciona como contador
    class Contador extends TimerTask {
        public void run() {
            segundos++;
            //System.out.println("segundo: " + segundos);
        }
    }
    //Crea un timer, inicia segundos a 0 y comienza a contar
    public void Contar()
    {
        this.segundos=0;
        timer = new Timer();
        timer.schedule(new Contador(), 0, 1000);
    }
    //Detiene el contador
    public void Detener() {
        timer.cancel();
    }
    //Metodo que retorna los segundos transcurridos
    public int getSegundos()
    {
        return this.segundos;
    }
}
