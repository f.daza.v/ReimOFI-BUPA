package com.reim.chinoy.reimapp;

import android.app.Application;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Chinoy on 16-12-2015.
 */
public class Reim extends AppCompatActivity{
    private String idReim[];
    private String nomReim[];

    public String[] getIdReim() {
        return idReim;
    }

    public String[] getNomReim() {
        return nomReim;
    }

    public void setNomReim(String[] nomReim) {
        this.nomReim = nomReim;
    }

    public void setIdReim(String[] idReim) {
        this.idReim = idReim;
    }
}
