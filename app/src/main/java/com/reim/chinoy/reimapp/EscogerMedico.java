package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EscogerMedico extends AppCompatActivity implements View.OnClickListener {


    Tiempo tiempo=new Tiempo();

    java.util.Date utilDate = new java.util.Date();
    java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

    MediaPlayer nosoyelmedico = new MediaPlayer();
    MediaPlayer cualeselmedico= new MediaPlayer();

    String query2="INSERT INTO resultados_reim (id_resultado, id_alumno, id_instrumento, id_actividad,id_item, id_alternativa, c_touch,c_audio,tiempo,fecha, hora, es_correcta)"+
            "VALUES(null,?,508,3,?,?,?,?,?,?,?,?)";
    /*String query="INSERT INTO resultado_reim_ofi (id_resultado, id_alumno, id_instrumento, id_actividad,id_ambiente, id_item, id_alternativa, c_touch,tiempo,fecha, hora, es_correcta, imagen)"+
            "VALUES(null,?,508,4,?,?,?,?,?,?,?,?,?)";*/ //1 id alumno 2 id_ambiente 3 id item 4 id alternativa 5 ctouch 6 tiempo 7 fecha, 8 hora,9 es correcta, 10 imagen
    String imagen="";

    SmartImageView avatarAlum;

    int aux=0;
    int toques=1;
    int item=1;
    int idalternativa=0;
    int time=0;
    int correcta=0;
    int idambiente=3;

    ImageButton altmedico1;
    ImageButton altmedico2;
    ImageButton altmedico3;
    ImageButton BtnAudio;
    ImageButton menu;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escoger_medico);

        Usuarios usuario = (Usuarios) getApplicationContext();
        usuario.setActividadOfi(3);

        nosoyelmedico  =  MediaPlayer.create(EscogerMedico.this,R.raw.no_soy_doctor);
        cualeselmedico =  MediaPlayer.create(EscogerMedico.this,R.raw.cual_es_doctor);

        altmedico1= (ImageButton) findViewById(R.id.emalt1);
        altmedico2= (ImageButton) findViewById(R.id.emalt2);
        altmedico3= (ImageButton) findViewById(R.id.emalt3);
        BtnAudio= (ImageButton)findViewById(R.id.EscAudiomedico);
        menu= (ImageButton) findViewById(R.id.emmenu);
        avatarAlum     = (SmartImageView) findViewById(R.id.avatarAlumEM);

        asignarAvatarAlumMB();

        altmedico1.setOnClickListener(this);
        altmedico2.setOnClickListener(this);
        altmedico3.setOnClickListener(this);
        menu.setOnClickListener(this);
        BtnAudio.setOnClickListener(this);


        tiempo.Contar();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.emalt1:

                imagen="bombero";
                idalternativa=1;
                time=tiempo.getSegundos();
                correcta=0;
                nosoyelmedico.start();
                consulta alt1 = new consulta();
                alt1.execute();

                break;

            case R.id.emalt2:
                imagen="medico";
                idalternativa=2;
                time=tiempo.getSegundos();
                correcta=1;
                consulta alt2 = new consulta();
                alt2.execute();

                tiempo.Detener();
                Intent intent = new Intent(EscogerMedico.this,EntregaListon1_OFi.class);
                startActivity(intent);

                break;

            case R.id.emalt3:
                imagen="chef";
                correcta=0;
                time=tiempo.getSegundos();
                idalternativa=3;
                nosoyelmedico.start();
                consulta alt3 = new consulta();
                alt3.execute();

                break;

            case R.id.EscAudiomedico:
                cualeselmedico.start();
                break;

            case R.id.emmenu:
                Intent intentmenu = new Intent(EscogerMedico.this,ReimOfi.class);
                startActivity(intentmenu);
        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void asignarAvatarAlumMB(){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/avatarUsuario/extraerAvatar.php?idUser=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        String img;
                        img = o.getString("avatar");
                        String url = "http://desarrolloreim.ulearnet.com/avatarUsuario/" + img + ".png";
                        Rect rect = new Rect(avatarAlum.getLeft(), avatarAlum.getTop(), avatarAlum.getRight(), avatarAlum.getBottom());
                        avatarAlum.setImageUrl(url, rect);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }


    private class consulta extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            altmedico1.setEnabled(false);
            altmedico2.setEnabled(false);
            altmedico3.setEnabled(false);


        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            altmedico1.setEnabled(true);
            altmedico2.setEnabled(true);
            altmedico3.setEnabled(true);

        }

        @Override
        protected Void doInBackground(Void... params) {
            Usuarios usuario = (Usuarios) getApplicationContext();
            int  idusuario= Integer.parseInt(usuario.getIdAlumno());

            try {

                Date utilhora = new Date();

                String mydate =java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                String [] fechahora=mydate.split(" ");

                SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                utilhora = formatter.parse(fechahora[1]);
                java.sql.Time sqlhora = new java.sql.Time(utilhora.getTime());

                Class.forName("com.mysql.jdbc.Driver");
                String url = "jdbc:mysql://mysql.ulearnet.com:3306/ulearnet_reim";//"jdbc:mysql:///10.0.3.2:3306/dbname"
                Connection c = DriverManager.getConnection(url, "fdaza", "felipe2016");//password_you_wish;
                PreparedStatement st = c.prepareStatement(query2);//"UPDATE toques SET touch = (?) WHERE toques.id = 10"

                //1 id alumno 2 id item 3 id alternativa 4 ctouch 5 tiempo 6 fecha, 7 hora,8 es correcta, 9 imagen

                st.setInt(1, idusuario);//22712
                st.setInt(2,item);
                st.setInt(3,idalternativa);
                st.setInt(4,toques);
                st.setInt(5,1);
                st.setInt(6, time);
                st.setDate(7, sqlDate);
                st.setTime(8, sqlhora);
                st.setInt(9, correcta);
                //st.setString(10,imagen);
                st.execute();
                st.close();
                c.close();
            }
            catch (ClassNotFoundException | SQLException | ParseException e){ //SQLException e
                e.printStackTrace();
            }




            // ResultadoReim(query);



            return null;
        }
    }



}
