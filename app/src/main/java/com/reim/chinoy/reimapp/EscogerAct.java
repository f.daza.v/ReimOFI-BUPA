package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;



public class EscogerAct extends AppCompatActivity {

    public ReimAct reim = new ReimAct();
    public CursoActivo curso = new CursoActivo();

    int bupaSelect = 1;
    int ofiSelect = 0;

    public String idCursos;
    public String nomCursos;

    public String[] idCur;
    public String[] nomCur;

    public ListView listaItems;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escoger);
        Usuarios usuario = (Usuarios) getApplicationContext();
        usuario.setvReim(0);
        usuario.setvCurso(0);

        asignarCursos();

        // Instanciamos las View

        listaItems = (ListView) findViewById(R.id.listaReimEscoger);
        listaItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String curso = nomCur[position];
                Crouton.makeText(EscogerAct.this, "Escogio el Curso: "+curso, Style.ALERT).show();
                Usuarios usuario = (Usuarios) getApplicationContext();
                //int idC = Integer.parseInt(idCur[position].trim());
                usuario.setCursoActivo(idCur[position]);
                usuario.setNomCursoActivo(curso);
                usuario.setvCurso(1);

            }
        });

    }


    //FUNCION PARA LABOR DE LAS FLECHAS. CAMBIA EL REIM QUE MUESTRA
    public void cambiarReim(View v){

        ImageButton botonBupa = (ImageButton) findViewById(R.id.escogerBupa);
        ImageButton botonOfi = (ImageButton) findViewById(R.id.escogerOfi);
        TextView texto = (TextView) findViewById(R.id.nomReimActual);
        TextView desTexto = (TextView) findViewById(R.id.descripcionReim);

        if(bupaSelect == 1){
            botonBupa.setVisibility(View.INVISIBLE);
            botonBupa.setFocusable(false);

            botonOfi.setVisibility(View.VISIBLE);
            botonOfi.setFocusable(true);

            texto.setText("OFI");
            desTexto.setText("Identificar los oficios.");
            ofiSelect = 1;
            bupaSelect = 0;
        }else{
            botonOfi.setVisibility(View.INVISIBLE);
            botonOfi.setFocusable(false);

            botonBupa.setVisibility(View.VISIBLE);
            botonBupa.setFocusable(true);
            texto.setText("BUPA");
            desTexto.setText("Identificar las palabras a traves de los fonemas.");
            ofiSelect = 0;
            bupaSelect = 1;
        }

    }

    //FUNCION PARA VER INFORMACION DEL REIM OFI
    public void reimOfi(View v){

        TextView texto = (TextView) findViewById(R.id.descripcionReim);
        texto.setText("Identificar los oficios.");
        Usuarios usuario = (Usuarios) getApplicationContext();
        usuario.setvReim(1);
        ofiSelect = 1;
        bupaSelect = 0;
    }

    //FUNCION PARA VER INFORMACION DEL REIM BUPA
    public void reimBupa(View v){

        TextView texto = (TextView) findViewById(R.id.descripcionReim);
        texto.setText("Identificar las palabras a traves de los fonemas.");
        Usuarios usuario = (Usuarios) getApplicationContext();
        usuario.setvReim(1);
        ofiSelect = 0;
        bupaSelect = 1;

    }

    //FUNCION PARA ESCOGER EL REIM
    public void escogerReim(View v){

        if (bupaSelect == 1) {
            reim.setIdReim("507");
            reim.setNomReim("BUPA");
            Crouton.makeText(EscogerAct.this, "Escogio el Reim BUPA.", Style.ALERT).show();
            Usuarios usuario = (Usuarios) getApplicationContext();
            usuario.setReimActivo("507");
            usuario.setNomReimActivo("BUPA");
            usuario.setvReim(1);
        }else {
            reim.setIdReim("508");
            reim.setNomReim("OFI");
            Crouton.makeText(EscogerAct.this, "Escogio el Reim OFI.", Style.ALERT).show();
            Usuarios usuario = (Usuarios) getApplicationContext();
            usuario.setReimActivo("508");
            usuario.setNomReimActivo("OFI");
            usuario.setvReim(1);
        }

    }

    public void asignarCursos(){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idEdu = usuario.getIdEducadora();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/escogerReim/cursosEdu.php?idEdu=" + idEdu, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {

                        JSONObject o = new JSONObject(new String(responseBody));
                        nomCursos = o.getString("nomCursos");
                        idCursos = o.getString("idCursos");

                        nomCur = nomCursos.split("[-]");
                        idCur = idCursos.split("[-]");

                        listaItems = (ListView) findViewById(R.id.listaReimEscoger);

                        // Creamos el ArrayAdapter<String>
                        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                getApplicationContext(), R.layout.list_black_text, R.id.list_content,
                                nomCur);
                        // Establecemos el Adapter
                        listaItems.setAdapter(adapter);


                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }


    public void escogerActividad(View v) {//escoger actividad
        Usuarios usuario = (Usuarios) getApplicationContext();
        int reim = usuario.getvReim();
        int curso = usuario.getvCurso();
       if(reim == 1){
           if(curso == 1){
               usuario.setvReim(0);
               usuario.setvCurso(0);
               Intent intent = new Intent(EscogerAct.this, EscogerAct2.class);
               startActivity(intent);
           }else{
               Crouton.makeText(EscogerAct.this, "Debe escoger un Curso.", Style.ALERT).show();
           }
       }else{
           Crouton.makeText(EscogerAct.this, "Debe escoger un Reim.", Style.ALERT).show();
       }
    }


}
