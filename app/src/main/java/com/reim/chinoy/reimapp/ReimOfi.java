package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

public class ReimOfi extends AppCompatActivity {


    SmartImageView avatarAlum;
    TextView nomAlumno;
    MediaPlayer escogeactividad= new MediaPlayer();
    MediaPlayer musica= new MediaPlayer();
    ImageButton menubombero;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_ofi);
        Usuarios usuario = (Usuarios) getApplicationContext();
        usuario.setReimActivo("508");
        escogeactividad=  MediaPlayer.create(ReimOfi.this,R.raw.escoge_una_actividad);
        musica=  MediaPlayer.create(ReimOfi.this,R.raw.menubupa);
        menubombero    = (ImageButton) findViewById(R.id.MenuBombero);
        avatarAlum     = (SmartImageView) findViewById(R.id.avatarMofi);



        String nomAlum = usuario.getNomAlumno();
        String apAlum = usuario.getApAlumno();
        String nomCom = ""+nomAlum+" "+apAlum+"";
        nomAlumno = (TextView) findViewById(R.id.nombreAlumnoMO);
        nomAlumno.setText(nomCom);





        asignarAvatarAlumMB();





    }


    public void cuartelBombero (View v){



        Intent intent = new Intent(ReimOfi.this, EscogerBombero.class);

        startActivity(intent);

    }

    public void hospital (View v){


        Intent intent = new Intent(ReimOfi.this, EscogerMedico.class);

        startActivity(intent);

    }

    public void construccion (View v){



       Intent intent= new Intent (ReimOfi.this, EscogerConstructor.class);
        startActivity(intent);

    }


    public void restaurant (View v){

        Intent intent= new Intent (ReimOfi.this, EscogerChef.class);
        startActivity(intent);



    }

    public void Audiomenu (View V){

        escogeactividad.start();

    }

    public void salirOFI (View v){

        Intent intent = new  Intent (ReimOfi.this, Temporal.class);

        startActivity(intent);


    }

    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void asignarAvatarAlumMB(){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/avatarUsuario/extraerAvatar.php?idUser=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        String img;
                        img = o.getString("avatar");
                        String url = "http://desarrolloreim.ulearnet.com/avatarUsuario/" + img + ".png";
                        Rect rect = new Rect(avatarAlum.getLeft(), avatarAlum.getTop(), avatarAlum.getRight(), avatarAlum.getBottom());
                        avatarAlum.setImageUrl(url, rect);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }




}
