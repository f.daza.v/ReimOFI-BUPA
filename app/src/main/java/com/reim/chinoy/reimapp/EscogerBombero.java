package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.graphics.Rect;
import android.media.Image;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.SimpleTimeZone;

public class EscogerBombero extends AppCompatActivity implements  View.OnClickListener {


    java.util.Date utilDate = new java.util.Date();
    java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

    SmartImageView avatarAlum;


    Tiempo tiempo=new Tiempo();

    MediaPlayer nosoybombero = new MediaPlayer();
    MediaPlayer cualesbombero= new MediaPlayer();

    ImageButton menu;

    String query2="INSERT INTO resultados_reim (id_resultado, id_alumno, id_instrumento, id_actividad,id_item, id_alternativa, c_touch,c_audio,tiempo,fecha, hora, es_correcta)"+
            "VALUES(null,?,508,1,?,?,?,?,?,?,?,?)";//1 id_alumno,2 id_item,3 id alternativa, 4 c _touch, 5 caudio, 6 tiempo,7 fecha, 8 hora, 9 es_correcta

    /*String query="INSERT INTO resultado_reim_ofi (id_resultado, id_alumno, id_instrumento, id_actividad,id_ambiente, id_item, id_alternativa, c_touch,tiempo,fecha, hora, es_correcta, imagen)"+
            "VALUES(null,?,508,4,?,?,?,?,?,?,?,?,?)";*/ //1 id alumno 2 id_ambiente 3 id item 4 id alternativa 5 ctouch 6 tiempo 7 fecha, 8 hora,9 es correcta, 10 imagen
    String imagen="";



    int aux=0;
    int toques=1;
    int item=1;
    int idalternativa=0;
    int time=0;
    int correcta=0;
    int idambiente=1;

    ImageButton btnAudio;
    ImageButton almenuofi;
    Button altbombero1;
    Button altbombero2;
    Button altbombero3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escoger_bombero);

        Usuarios usuario = (Usuarios) getApplicationContext();
        usuario.setActividadOfi(1);

        nosoybombero  =  MediaPlayer.create(EscogerBombero.this,R.raw.no_soy_bombero);
        cualesbombero = MediaPlayer.create(EscogerBombero.this,R.raw.cual_es_bombero);

        avatarAlum     = (SmartImageView) findViewById(R.id.avatarAlumEB);

        asignarAvatarAlumMB();

        altbombero1= (Button) findViewById(R.id.ebalt1);
        altbombero2= (Button) findViewById(R.id.ebalt2);
        altbombero3= (Button) findViewById(R.id.ebalt3);
        almenuofi  = (ImageButton) findViewById(R.id.ebmenu);

        btnAudio=(ImageButton) findViewById(R.id.EscbomAudio);

        altbombero1.setOnClickListener(this);
        altbombero2.setOnClickListener(this);
        altbombero3.setOnClickListener(this);
        almenuofi.setOnClickListener(this);
        btnAudio.setOnClickListener(this);

        tiempo.Contar();







    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.ebalt1:
                imagen="chef";
                idalternativa=1;
                time=tiempo.getSegundos();
                correcta=1;
                nosoybombero.start();
                consulta alt1 = new consulta();
                alt1.execute();

                break;

            case R.id.ebalt2:
                imagen="bombero";
                idalternativa=2;
                time=tiempo.getSegundos();
                correcta=2;
                consulta alt2 = new consulta();
                alt2.execute();

                tiempo.Detener();
                Intent intent = new Intent(EscogerBombero.this,EntregaListon1_OFi.class);
                startActivity(intent);

                break;

            case R.id.ebalt3:
                imagen="medico";
                correcta=1;
                time=tiempo.getSegundos();
                idalternativa=3;
                nosoybombero.start();
                consulta alt3 = new consulta();
                alt3.execute();
                break;

            case R.id.EscbomAudio:

                cualesbombero.start();

                break;
            case R.id.ebmenu:
                Intent inten2 = new Intent(EscogerBombero.this,ReimOfi.class);
                startActivity(inten2);
                break;

        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void asignarAvatarAlumMB(){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/avatarUsuario/extraerAvatar.php?idUser=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        String img;
                        img = o.getString("avatar");
                        String url = "http://desarrolloreim.ulearnet.com/avatarUsuario/" + img + ".png";
                        Rect rect = new Rect(avatarAlum.getLeft(), avatarAlum.getTop(), avatarAlum.getRight(), avatarAlum.getBottom());
                        avatarAlum.setImageUrl(url, rect);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }




    //aca cargo la query en otro hilo para conectarme a internet
    private class consulta extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            altbombero1.setEnabled(false);
            altbombero2.setEnabled(false);
            altbombero3.setEnabled(false);
            btnAudio.setEnabled(false);

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            altbombero1.setEnabled(true);
            altbombero2.setEnabled(true);
            altbombero3.setEnabled(true);
            btnAudio.setEnabled(true);
        }


        //ESTE HACE LA CONSULTA POR DEBAJO
        @Override
        protected Void doInBackground(Void... params) {
            Usuarios usuario = (Usuarios) getApplicationContext();
            int  idusuario= Integer.parseInt(usuario.getIdAlumno());

            try {
                Date utilhora = new Date();

                String mydate =java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                String [] fechahora=mydate.split(" ");

                SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                utilhora = formatter.parse(fechahora[1]);
                java.sql.Time sqlhora = new java.sql.Time(utilhora.getTime());

                Class.forName("com.mysql.jdbc.Driver");
                String url = "jdbc:mysql://mysql.ulearnet.com:3306/ulearnet_reim";//"jdbc:mysql:///10.0.3.2:3306/dbname"
                Connection c = DriverManager.getConnection(url, "fdaza", "felipe2016");//password_you_wish;
                PreparedStatement st = c.prepareStatement(query2);//"UPDATE toques SET touch = (?) WHERE toques.id = 10"

                //1 id alumno 2 id item 3 id alternativa 4 ctouch 5 tiempo 6 fecha, 7 hora,8 es correcta, 9 imagen
                //1 id_alumno,2 id_item,3 id alternativa, 4 c _touch, 5 caudio, 6 tiempo,7 fecha, 8 hora, 9 es_correcta



                st.setInt(1, idusuario);//22712
                st.setInt(2,item);
                st.setInt(3,idalternativa);
                st.setInt(4,toques);
                st.setInt(5,1);
                st.setInt(6,time);
                st.setDate(7, sqlDate);
                st.setTime(8, sqlhora);
                st.setInt(9,correcta);
                st.execute();
                st.close();
                c.close();
            }
            catch (ClassNotFoundException | SQLException | ParseException e){ //SQLException e
                e.printStackTrace();
            }




            // ResultadoReim(query);



            return null;
        }
    }
}
