package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.internal.app.ToolbarActionBar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class EscogerConstructor extends AppCompatActivity implements View.OnClickListener{
    Tiempo tiempo = new Tiempo();

    java.util.Date utilDate = new java.util.Date();
    java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

    MediaPlayer nosoyconstructor = new MediaPlayer();
    MediaPlayer cualesconstructor= new MediaPlayer();


    String query2="INSERT INTO resultados_reim (id_resultado, id_alumno, id_instrumento, id_actividad,id_item, id_alternativa, c_touch,c_audio,tiempo,fecha, hora, es_correcta)"+
            "VALUES(null,?,508,4,?,?,?,?,?,?,?,?)";

    /*String query = "INSERT INTO resultado_reim_ofi (id_resultado, id_alumno, id_instrumento, id_actividad,id_ambiente, id_item, id_alternativa, c_touch,tiempo,fecha, hora, es_correcta, imagen)" +
            "VALUES(null,?,508,4,?,?,?,?,?,?,?,?,?)";*/ //1 id alumno 2 id_ambiente 3 id item 4 id alternativa 5 ctouch 6 tiempo 7 fecha, 8 hora,9 es correcta, 10 imagen
    String imagen = "";

    SmartImageView avatarAlum;

    int aux = 0;
    int toques = 1;
    int item = 1;
    int idalternativa = 0;
    int time = 0;
    int correcta = 0;
    int idambiente = 4;

    ImageButton altconstru1;
    ImageButton altconstru2;
    ImageButton altconstru3;
    ImageButton BtnAudio;
    ImageButton menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_escoger_constructor);

        Usuarios usuario = (Usuarios) getApplicationContext();
        usuario.setActividadOfi(4);

        nosoyconstructor  =  MediaPlayer.create(EscogerConstructor.this,R.raw.no_soy_constructor);
        cualesconstructor = MediaPlayer.create(EscogerConstructor.this,R.raw.cual_es_constructor);

        altconstru1 = (ImageButton) findViewById(R.id.ectalt1);
        altconstru2 = (ImageButton) findViewById(R.id.ectalt2);
        altconstru3 = (ImageButton) findViewById(R.id.ectalt3);
        BtnAudio    = (ImageButton) findViewById(R.id.EscconstruAudio);

        menu     = (ImageButton) findViewById(R.id.ectmenu);
        avatarAlum     = (SmartImageView) findViewById(R.id.avatarAlumnoEct);

        asignarAvatarAlumMB();

        altconstru1.setOnClickListener(this);
        altconstru2.setOnClickListener(this);
        altconstru3.setOnClickListener(this);
        menu.setOnClickListener(this);
        BtnAudio.setOnClickListener(this);

        tiempo.Contar();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ectalt1:

                imagen="bombero";
                idalternativa=1;
                time=tiempo.getSegundos();
                correcta=1;
                nosoyconstructor.start();
                consulta alt1 = new consulta();
                alt1.execute();


                break;

            case R.id.ectalt2:
                imagen="medico";
                idalternativa=2;
                time=tiempo.getSegundos();
                correcta=1;
                nosoyconstructor.start();
                consulta alt2 = new consulta();
                alt2.execute();




                break;

            case R.id.ectalt3:

                imagen="constructor";
                correcta=2;
                time=tiempo.getSegundos();
                idalternativa=3;
                consulta alt3 = new consulta();
                alt3.execute();
                tiempo.Detener();
                Intent intent = new Intent(EscogerConstructor.this,EntregaListon1_OFi.class);
                startActivity(intent);

                break;

            case R.id.EscconstruAudio:
                cualesconstructor.start();
                break;

            case R.id.ectmenu:
                Intent intentmenu= new Intent(EscogerConstructor.this, ReimOfi.class);
                startActivity(intentmenu);
        }

    }

    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void asignarAvatarAlumMB(){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/avatarUsuario/extraerAvatar.php?idUser=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        String img;
                        img = o.getString("avatar");
                        String url = "http://desarrolloreim.ulearnet.com/avatarUsuario/" + img + ".png";
                        Rect rect = new Rect(avatarAlum.getLeft(), avatarAlum.getTop(), avatarAlum.getRight(), avatarAlum.getBottom());
                        avatarAlum.setImageUrl(url, rect);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }


    private class consulta extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            altconstru1.setEnabled(false);
            altconstru2.setEnabled(false);
            altconstru3.setEnabled(false);
            BtnAudio.setEnabled(false);


        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            altconstru1.setEnabled(true);
            altconstru2.setEnabled(true);
            altconstru3.setEnabled(true);
            BtnAudio.setEnabled(true);

        }

        @Override
        protected Void doInBackground(Void... params) {
            Usuarios usuario = (Usuarios) getApplicationContext();
            int  idusuario= Integer.parseInt(usuario.getIdAlumno());

            try {

                Date utilhora = new Date();

                String mydate =java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                String [] fechahora=mydate.split(" ");

                SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                utilhora = formatter.parse(fechahora[1]);
                java.sql.Time sqlhora = new java.sql.Time(utilhora.getTime());

                Class.forName("com.mysql.jdbc.Driver");
                String url = "jdbc:mysql://mysql.ulearnet.com:3306/ulearnet_reim";//"jdbc:mysql:///10.0.3.2:3306/dbname"
                Connection c = DriverManager.getConnection(url, "fdaza", "felipe2016");//password_you_wish;
                PreparedStatement st = c.prepareStatement(query2);//"UPDATE toques SET touch = (?) WHERE toques.id = 10"

                //1 id alumno 2 id item 3 id alternativa 4 ctouch 5 tiempo 6 fecha, 7 hora,8 es correcta, 9 imagen

                st.setInt(1, idusuario);//22712
                st.setInt(2,item);
                st.setInt(3,idalternativa);
                st.setInt(4,toques);
                st.setInt(5,1);
                st.setInt(6, time);
                st.setDate(7, sqlDate);
                st.setTime(8, sqlhora);
                st.setInt(9, correcta);
                //st.setString(10, imagen);
                st.execute();
                st.close();
                c.close();
            } catch (ClassNotFoundException | SQLException | ParseException e) { //SQLException e
                e.printStackTrace();
            }

            return null;
        }


    }



}
