package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

public class EntregaListon3_OFI extends AppCompatActivity {
    MediaPlayer entregaliston;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entrega_liston3__ofi);

        entregaliston = MediaPlayer.create(this, R.raw.sonidomedalla);
        entregaliston.start();




    }
    public void flechaverdeOfi3(View v){
        Usuarios usuario = (Usuarios) getApplicationContext();
        switch (usuario.getActividadOfi()){
            case 1:
                Intent intent1 = new Intent(EntregaListon3_OFI.this,ReimOfi.class);
                startActivity(intent1);
                break;
            case 2:
                Intent intent2 = new Intent(EntregaListon3_OFI.this,ReimOfi.class);
                startActivity(intent2);
                break;
            case 3:
                Intent intent3 = new Intent(EntregaListon3_OFI.this,ReimOfi.class);
                startActivity(intent3);
                break;
            case 4:
                Intent intent4 = new Intent(EntregaListon3_OFI.this,ReimOfi.class);
                startActivity(intent4);
                break;

        }



    }

    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
