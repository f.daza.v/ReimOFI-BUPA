package com.reim.chinoy.reimapp;

import android.app.Application;

/**
 * Created by Bock on 27-01-2016.
 */
public class Usuarios extends Application {

    private String idEducadora;
    private String nomEducadora;
    private String apeEducadora;

    private String idAlumno;
    private String nomAlumno;
    private String apAlumno;

    private String cursoActivo;
    private String nomCursoActivo;
    private String reimActivo;
    private String nomReimActivo;

    private int actividadOfi;
    private int vConfirmacion = 0;
    private int vReim = 0;
    private int vCurso = 0;
    private int vAlumno = 0;


    public String getIdEducadora() {
        return idEducadora;
    }

    public void setIdEducadora(String idEducadora) {
        this.idEducadora = idEducadora;
    }

    public String getNomEducadora() {
        return nomEducadora;
    }

    public void setNomEducadora(String nomEducadora) {
        this.nomEducadora = nomEducadora;
    }

    public String getApeEducadora() {
        return apeEducadora;
    }

    public void setApeEducadora(String apeEducadora) {
        this.apeEducadora = apeEducadora;
    }

    public String getIdAlumno() {
        return idAlumno;
    }

    public void setIdAlumno(String idAlumno) {
        this.idAlumno = idAlumno;
    }

    public String getNomAlumno() {
        return nomAlumno;
    }

    public void setNomAlumno(String nomAlumno) {
        this.nomAlumno = nomAlumno;
    }

    public String getApAlumno() {
        return apAlumno;
    }

    public void setApAlumno(String apAlumno) {
        this.apAlumno = apAlumno;
    }


    public String getCursoActivo() {return cursoActivo;}

    public void setCursoActivo(String cursoActivo) {this.cursoActivo = cursoActivo; }

    public String getReimActivo() { return reimActivo; }

    public void setReimActivo(String reimActivo) { this.reimActivo = reimActivo; }

    public String getNomCursoActivo() {return nomCursoActivo; }

    public void setNomCursoActivo(String nomCursoActivo) { this.nomCursoActivo = nomCursoActivo;  }

    public String getNomReimActivo() { return nomReimActivo;  }

    public void setNomReimActivo(String nomReimActivo) { this.nomReimActivo = nomReimActivo; }

    public int getvConfirmacion() {
        return vConfirmacion;
    }

    public void setvConfirmacion(int vConfirmacion) {
        this.vConfirmacion = vConfirmacion;
    }

    public int getvReim() {
        return vReim;
    }

    public void setvReim(int vReim) {
        this.vReim = vReim;
    }

    public int getvCurso() {
        return vCurso;
    }

    public void setvCurso(int vCurso) {
        this.vCurso = vCurso;
    }

    public int getvAlumno() {
        return vAlumno;
    }

    public void setvAlumno(int vAlumno) {
        this.vAlumno = vAlumno;
    }

    public int getActividadOfi() {
        return actividadOfi;
    }

    public void setActividadOfi(int actividadOfi) {
        this.actividadOfi = actividadOfi;
    }
}
