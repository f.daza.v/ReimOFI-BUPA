package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

/**
 * Created by Bock on 01/06/2016.
 */
public class InfoAct extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_actividad);
        asignarInfo();
    }

    public void asignarInfo(){

        Usuarios usuario = (Usuarios) getApplicationContext();
        String idEdu = usuario.getIdEducadora();


        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/menuPrincipal/extraerPininfo.php?idEdu=" + idEdu, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        String pin = o.getString("pin");
                        String curso = o.getString("curso");
                        int reim = o.getInt("reim");

                        TextView t1 = (TextView) findViewById(R.id.pinInfo);
                        TextView t2 = (TextView) findViewById(R.id.nomReimInfo);
                        TextView t3 = (TextView) findViewById(R.id.cursoInfo);
                        ImageView img = (ImageView) findViewById(R.id.reimInfo);
                        t1.setText(pin);

                        t3.setText(curso);

                        if(reim == 507){
                            img.setImageResource(R.drawable.bupacon);
                            t2.setText("BUPA");
                        }
                        if(reim == 508){
                            img.setImageResource(R.drawable.oficon);
                            t2.setText("OFI");
                        }


                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void volverMinfo(View v){
        Intent intent = new Intent(InfoAct.this, MenuPrin.class);
        startActivity(intent);
    }
}
