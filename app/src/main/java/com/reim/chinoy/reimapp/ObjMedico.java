package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ObjMedico extends AppCompatActivity implements View.OnClickListener {

    MediaPlayer noesloquebusco = new MediaPlayer();
    MediaPlayer heperdidocosas = new MediaPlayer();
    MediaPlayer loquebuscaba   = new MediaPlayer();
    Tiempo tiempo=new Tiempo();

    ImageButton alt1;
    ImageButton alt2;
    ImageButton alt3;
    ImageButton alt4;
    ImageButton menu;
    ImageButton BtnAudio;

    SmartImageView avatarAlum;

    java.util.Date utilDate = new java.util.Date();
    java.sql.Date  sqlDate  = new java.sql.Date(utilDate.getTime());

    int item=1;
    int idalternativa=1;
    int toques=1;
    int time=0;
    int correcta=0;
    int idambiente=3;

    String query2="INSERT INTO resultados_reim (id_resultado, id_alumno, id_instrumento, id_actividad,id_item, id_alternativa, c_touch,c_audio,tiempo,fecha, hora, es_correcta)"+
            "VALUES(null,?,508,3,?,?,?,?,?,?,?,?)";
    /*y="INSERT INTO resultado_reim_ofi (id_resultado, id_alumno, id_instrumento, id_actividad,id_ambiente, id_item, id_alternativa, c_touch,tiempo,fecha, hora, es_correcta, imagen)"+
            "VALUES(null,?,508,5,?,?,?,?,?,?,?,?,?)";*/ //1 id alumno 2 id_ambiente 3 id item 4 id alternativa 5 ctouch 6 tiempo 7 fecha, 8 hora,9 es correcta, 10 imagen
    String imagen="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_obj_medico);


        noesloquebusco= MediaPlayer.create(ObjMedico.this, R.raw.no_es_lo_que_busco);
        heperdidocosas= MediaPlayer.create(ObjMedico.this, R.raw.se_me_han_perdido_mis_cosas);
        loquebuscaba= MediaPlayer.create(ObjMedico.this, R.raw.justo_lo_que_buscaba);

        avatarAlum     = (SmartImageView) findViewById(R.id.avatarAlumObjM);
        asignarAvatarAlumMB();

        tiempo.Contar();

        alt1=(ImageButton)findViewById(R.id.objmalt1);
        alt2=(ImageButton)findViewById(R.id.objmalt2);
        alt3=(ImageButton)findViewById(R.id.objmalt3);
        alt4=(ImageButton)findViewById(R.id.objmalt4);
        menu=(ImageButton)findViewById(R.id.objmMenu);
        BtnAudio=(ImageButton)findViewById(R.id.MobjAudio);


        alt1.setOnClickListener(this);
        alt2.setOnClickListener(this);
        alt3.setOnClickListener(this);
        alt4.setOnClickListener(this);
        menu.setOnClickListener(this);
        BtnAudio.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){

            case R.id.objmalt1:

                idalternativa=1;

                if(item==2){

                    imagen = "jeringa";
                    time = tiempo.getSegundos();
                    correcta = 2;
                    loquebuscaba.start();
                    consulta qalt2 = new consulta();
                    qalt2.execute();

                    alt1.setBackgroundResource(R.drawable.guitarra);
                    alt2.setBackgroundResource(R.drawable.botiquin);
                    alt3.setBackgroundResource(R.drawable.lampara);
                    alt4.setBackgroundResource(R.drawable.juguera);
                    item++;
                }else{
                    switch(item){
                        case 1:
                            imagen="lampara";
                            correcta=1;
                            time=tiempo.getSegundos();
                            consulta a1 = new consulta();
                            noesloquebusco.start();
                            a1.execute();
                            break;

                        case 3:
                            imagen="guitarra";
                            correcta=1;
                            time=tiempo.getSegundos();
                            consulta b1 = new consulta();
                            noesloquebusco.start();
                            b1.execute();
                            break;

                        case 4:
                            imagen="guitarra";
                            correcta=1;
                            time=tiempo.getSegundos();
                            consulta c1 = new consulta();
                            noesloquebusco.start();
                            c1.execute();
                            break;


                    }

                }




                break;

            case R.id.objmalt2:

                idalternativa=2;

                if(item==3){

                    imagen = "botiquin";
                    time = tiempo.getSegundos();
                    correcta = 2;
                    loquebuscaba.start();
                    consulta qalt2 = new consulta();
                    qalt2.execute();

                    alt1.setBackgroundResource(R.drawable.guitarra);
                    alt2.setBackgroundResource(R.drawable.juguera);
                    alt3.setBackgroundResource(R.drawable.lampara);
                    alt4.setBackgroundResource(R.drawable.camilla);
                    item++;
                }else{
                    switch(item){
                        case 1:
                            imagen="guitarra";
                            correcta=1;
                            time=tiempo.getSegundos();
                            consulta a1 = new consulta();
                            noesloquebusco.start();
                            a1.execute();
                            break;

                        case 2:
                            imagen="lampara";
                            correcta=1;
                            time=tiempo.getSegundos();
                            consulta b1 = new consulta();
                            noesloquebusco.start();
                            b1.execute();
                            break;

                        case 4:
                            imagen="juguera";
                            correcta=1;
                            time=tiempo.getSegundos();
                            consulta c1 = new consulta();
                            noesloquebusco.start();
                            c1.execute();
                            break;


                    }
                }



                break;

            case R.id.objmalt3:
                idalternativa=3;

                if(item==1) {

                    imagen = "estetoscopio";
                    time = tiempo.getSegundos();
                    correcta = 2;
                    loquebuscaba.start();
                    consulta qalt3 = new consulta();
                    qalt3.execute();

                    alt1.setBackgroundResource(R.drawable.jeringa);
                    alt2.setBackgroundResource(R.drawable.lampara);
                    alt3.setBackgroundResource(R.drawable.juguera);
                    alt4.setBackgroundResource(R.drawable.guitarra);
                    item++;
                }else{
                    switch(item){
                        case 2:
                            imagen="juguera";
                            correcta=1;
                            time=tiempo.getSegundos();
                            consulta a1 = new consulta();
                            noesloquebusco.start();
                            a1.execute();
                            break;

                        case 3:
                            imagen="lampara";
                            correcta=1;
                            time=tiempo.getSegundos();
                            consulta b1 = new consulta();
                            noesloquebusco.start();
                            b1.execute();
                            break;

                        case 4:
                            imagen="juguera";
                            correcta=1;
                            time=tiempo.getSegundos();
                            consulta c1 = new consulta();
                            noesloquebusco.start();
                            c1.execute();
                            break;


                    }
                }

                break;

            case R.id.objmalt4:
                idalternativa=4;

                if(item==4) {

                    imagen = "camilla";
                    time = tiempo.getSegundos();
                    correcta = 2;
                    loquebuscaba.start();
                    consulta qalt4 = new consulta();
                    qalt4.execute();

                    Intent intent = new Intent(ObjMedico.this, EntregaListon2_OFI.class);

                    startActivity(intent);

                }else{
                    switch(item){
                        case 1:
                            imagen="juguera";
                            correcta=1;
                            time=tiempo.getSegundos();
                            consulta a1 = new consulta();
                            noesloquebusco.start();
                            a1.execute();
                            break;

                        case 2:
                            imagen="guitarra";
                            correcta=1;
                            time=tiempo.getSegundos();
                            consulta b1 = new consulta();
                            noesloquebusco.start();
                            b1.execute();
                            break;

                        case 3:
                            imagen="guitarra";
                            correcta=1;
                            time=tiempo.getSegundos();
                            consulta c1 = new consulta();
                            noesloquebusco.start();
                            c1.execute();
                            break;


                    }
                }

                break;

            case R.id.MobjAudio:
                heperdidocosas.start();

                break;

            case R.id.objmMenu:
                Intent intentmenu =new Intent(ObjMedico.this,ReimOfi.class);
                startActivity(intentmenu);
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void asignarAvatarAlumMB(){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/avatarUsuario/extraerAvatar.php?idUser=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        String img;
                        img = o.getString("avatar");
                        String url = "http://desarrolloreim.ulearnet.com/avatarUsuario/" + img + ".png";
                        Rect rect = new Rect(avatarAlum.getLeft(), avatarAlum.getTop(), avatarAlum.getRight(), avatarAlum.getBottom());
                        avatarAlum.setImageUrl(url, rect);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    private class consulta extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();


            alt1.setEnabled(false);
            alt2.setEnabled(false);
            alt3.setEnabled(false);
            alt4.setEnabled(false);

        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            alt1.setEnabled(true);
            alt2.setEnabled(true);
            alt3.setEnabled(true);
            alt4.setEnabled(true);
        }

        @Override
        protected Void doInBackground(Void... params) {
            Usuarios usuario = (Usuarios) getApplicationContext();
            int  idusuario= Integer.parseInt(usuario.getIdAlumno());

            try {
                Date utilhora = new Date();

                String mydate =java.text.DateFormat.getDateTimeInstance().format(Calendar.getInstance().getTime());
                String [] fechahora=mydate.split(" ");

                SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                utilhora = formatter.parse(fechahora[1]);
                java.sql.Time sqlhora = new java.sql.Time(utilhora.getTime());

                Class.forName("com.mysql.jdbc.Driver");
                String url = "jdbc:mysql://mysql.ulearnet.com:3306/ulearnet_reim";//"jdbc:mysql:///10.0.3.2:3306/dbname"
                Connection c = DriverManager.getConnection(url, "fdaza", "felipe2016");//password_you_wish;
                PreparedStatement st = c.prepareStatement(query2);
                //1 id alumno 2 id_ambiente 3 id item 4 id alternativa 5 ctouch 6 tiempo 7 fecha, 8 hora,9 es correcta, 10 imagen

                st.setInt(1, idusuario);//22712
                st.setInt(2,item);
                st.setInt(3,idalternativa);
                st.setInt(4,toques);
                st.setInt(5,1);
                st.setInt(6, time);
                st.setDate(7, sqlDate);
                st.setTime(8, sqlhora);
                st.setInt(9, correcta);

                st.execute();
                st.close();
                c.close();
            }
            catch (ClassNotFoundException | SQLException | ParseException e){ //SQLException e
                e.printStackTrace();
            }




            // ResultadoReim(query);



            return null;
        }
    }

}
