package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.graphics.Rect;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONObject;

/**
 * Created by Bock on 22-01-2016.
 */
public class ReimBupa extends AppCompatActivity {
    SmartImageView avatarAlum;
    public MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_bupa);
        avatarAlum = (SmartImageView) findViewById(R.id.avatarAlumMR);
        mediaPlayer = MediaPlayer.create(this, R.raw.menubupa);
        mediaPlayer.start();
        asignarAvatarAlumMB();
        ingresarRegistroMB();

        TextView nomEducadora;

        Usuarios usuario = (Usuarios) getApplicationContext();
        String nomAlum = usuario.getNomAlumno();
        String idAlum = usuario.getIdAlumno();
        String apAlum = usuario.getApAlumno();

        String nomCom = ""+nomAlum+" "+apAlum+"";

        nomEducadora = (TextView) findViewById(R.id.nomAlumMB);
        nomEducadora.setText(nomCom);

    }

    public void asignarAvatarAlumMB(){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/avatarUsuario/extraerAvatar.php?idUser=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        String img;
                        img = o.getString("avatar");
                        String url = "http://desarrolloreim.ulearnet.com/avatarUsuario/" + img + ".png";
                        Rect rect = new Rect(avatarAlum.getLeft(), avatarAlum.getTop(), avatarAlum.getRight(), avatarAlum.getBottom());
                        avatarAlum.setImageUrl(url, rect);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void ingresarRegistroMB(){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/menuPrin/verificarRegistro.php?idAlum=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {



                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void salirReimBupa(View v){
        mediaPlayer.stop();
        Intent intent = new Intent(ReimBupa.this, Temporal.class);
        startActivity(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    public void bupaActividad1(View v){
        mediaPlayer.release();
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/menuPrin/extraerEtapaAct1.php?idAlum=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        int res;
                        JSONObject o = new JSONObject(new String(responseBody));
                        res = o.getInt("etapa");

                        System.gc();
                        finish();
                        if(res == 1){
                            Intent intent = new Intent(ReimBupa.this, BupaAct1.class);
                            startActivity(intent);
                        }
                        if(res == 2){
                            Intent intent = new Intent(ReimBupa.this, BupaAct12.class);
                            startActivity(intent);
                        }
                        if(res == 3){
                            Intent intent = new Intent(ReimBupa.this, BupaAct13.class);
                            startActivity(intent);
                        }
                        if (res == 4) {
                            Intent intent = new Intent(ReimBupa.this, BupaAct14.class);
                            startActivity(intent);
                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void bupaActividad2(View v){
        mediaPlayer.release();
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/menuPrin/extraerEtapaAct2.php?idAlum=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        int res;
                        JSONObject o = new JSONObject(new String(responseBody));
                        res = o.getInt("etapa");

                        System.gc();
                        finish();

                        if (res == 1) {
                            Intent intent = new Intent(ReimBupa.this, BupaAct2.class);
                            startActivity(intent);
                        }
                        if (res == 2) {
                            Intent intent = new Intent(ReimBupa.this, BupaAct22.class);
                            startActivity(intent);
                        }
                        if (res == 3) {
                            Intent intent = new Intent(ReimBupa.this, BupaAct23.class);
                            startActivity(intent);
                        }
                        if (res == 4) {
                            Intent intent = new Intent(ReimBupa.this, BupaAct24.class);
                            startActivity(intent);
                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void bupaActividad3(View v){
        mediaPlayer.release();
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/menuPrin/extraerEtapaAct3.php?idAlum=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        int res;
                        JSONObject o = new JSONObject(new String(responseBody));
                        res = o.getInt("etapa");

                        System.gc();
                        finish();

                        if (res == 1) {
                            Intent intent = new Intent(ReimBupa.this, BupaAct3.class);
                            startActivity(intent);
                        }
                        if (res == 2) {
                            Intent intent = new Intent(ReimBupa.this, BupaAct32.class);
                            startActivity(intent);
                        }
                        if (res == 3) {
                            Intent intent = new Intent(ReimBupa.this, BupaAct33.class);
                            startActivity(intent);
                        }
                        if (res == 4) {
                            Intent intent = new Intent(ReimBupa.this, BupaAct34.class);
                            startActivity(intent);
                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void bupaActividadCerrar(View v){
        Intent intent = new Intent(Intent.ACTION_MAIN); finish();
    }

    public void trofeosMenu(View v){
        mediaPlayer.stop();
        Intent intent = new Intent(ReimBupa.this, BupaTrofeos.class);
        startActivity(intent);
    }
}
