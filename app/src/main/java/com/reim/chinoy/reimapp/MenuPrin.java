package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.snowdream.android.widget.SmartImageView;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONException;
import org.json.JSONObject;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;

public class MenuPrin extends AppCompatActivity {
    SmartImageView avatarEdu;
    @Override
    //Creador de Vista + Inserta nombre de usuario
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_prin);
        avatarEdu = (SmartImageView) findViewById(R.id.avatarEduMP);
        asignarAvatar();
        EditText nomEducadora;

        Usuarios usuario = (Usuarios) getApplicationContext();
        String nomEdu = usuario.getNomEducadora();
        String idEdu = usuario.getIdEducadora();
        String apEdu = usuario.getApeEducadora();

        String nomCom = ""+nomEdu+" "+apEdu+"";

        nomEducadora = (EditText) findViewById(R.id.nombreEducadora);
        nomEducadora.setText(nomCom);
    }

    public void asignarAvatar(){
        String url = "http://desarrolloreim.ulearnet.com/avatarUsuario/usuario.png";
        Rect rect = new Rect(avatarEdu.getLeft(), avatarEdu.getTop(), avatarEdu.getRight(), avatarEdu.getBottom());
        avatarEdu.setImageUrl(url, rect);
    }

    public void verResultados(View v){
        Intent intent = new Intent(MenuPrin.this, Resultados.class);
        startActivity(intent);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    //Inicia vista "Inicar Actividad" y verificar actividades.
    public void loginAct(View v) {
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idEdu = usuario.getIdEducadora();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/iniciarAct/verActividad.php?idEdu=" + idEdu, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        int res = o.getInt("respuesta");

                        if(res == 1){
                            Crouton.makeText(MenuPrin.this, "No mantiene una actividad en curso.", Style.ALERT).show();
                        }else{
                            if(res == 2){
                                Intent intent = new Intent(MenuPrin.this, LoginActividad.class);
                                startActivity(intent);
                            }else{
                                Crouton.makeText(MenuPrin.this, "Mantiene una actividad en curso que ya termino su tiempo. Ha sido terminada automaticamente.", Style.ALERT).show();
                            }
                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });

    }

    //Iniciar vist "Escoger Actividad"
    public void escActMP(View v) {

        Usuarios usuario = (Usuarios) getApplicationContext();
        String idEdu = usuario.getIdEducadora();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/escogerReim/verificarAct.php?idEdu=" + idEdu, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        int res = o.getInt("respuesta");

                        if(res == 1){
                            Intent intent = new Intent(MenuPrin.this, EscogerAct.class);
                            startActivity(intent);
                        }else{
                            if(res == 2){
                                Crouton.makeText(MenuPrin.this, "Ya mantiene una actividad en curso.", Style.ALERT).show();
                            }else{
                                Crouton.makeText(MenuPrin.this, "Mantiene una actividad en curso que ya termino su tiempo. Ha sido terminada automaticamente.", Style.ALERT).show();
                            }
                        }

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });


    }
    //Logout de la App
    public void logout(View v) {
        Intent intent = new Intent(MenuPrin.this, MainActivity.class);
        startActivity(intent);
    }

    //Metodo para terminar la actividad en curso.
    public void terminar(View v) {

        Usuarios usuario = (Usuarios) getApplicationContext();
        String eduActiva = usuario.getIdEducadora();

        AsyncHttpClient client = new AsyncHttpClient();
        String url = "http://desarrolloreim.ulearnet.com/terminarAct/terminarAct.php";

        //Reuno datos para hacer el post
        RequestParams requestParams = new RequestParams();
        requestParams.add("id", eduActiva);

        RequestHandle post = client.post(url, requestParams, new AsyncHttpResponseHandler() {
            int resphp = 0;
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(statusCode==200){
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        resphp = o.getInt("respuesta");

                        if(resphp == 1){
                            Crouton.makeText(MenuPrin.this,"Su actividad ha sido terminada exitosamente.", Style.ALERT).show();
                        }
                        if(resphp == 2){
                            Crouton.makeText(MenuPrin.this,"No mantiene actividades en curso actualmente.", Style.ALERT).show();
                        }
                    }catch (JSONException e) {  }

                }else{
                    Crouton.makeText(MenuPrin.this,"Error de conexion al servidor.", Style.ALERT).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable throwable) {

            }

        });
    }

    public void infoAct(View v){
        Usuarios usuario = (Usuarios) getApplicationContext();
        String eduActiva = usuario.getIdEducadora();

        AsyncHttpClient client = new AsyncHttpClient();
        String url = "http://desarrolloreim.ulearnet.com/menuPrincipal/verificarAct.php";

        RequestParams requestParams = new RequestParams();
        requestParams.add("idEducadora", eduActiva);

        RequestHandle post = client.post(url, requestParams, new AsyncHttpResponseHandler() {
            int resphp = 0;
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(statusCode==200){
                    try {
                        JSONObject o = new JSONObject(new String(responseBody));
                        resphp = o.getInt("respuesta");

                        if(resphp == 1){
                            Crouton.makeText(MenuPrin.this,"No mantiene una actividad activa en estos momentos.", Style.ALERT).show();
                        }
                        if(resphp == 2){
                            Intent intent = new Intent(MenuPrin.this, InfoAct.class);
                            startActivity(intent);
                        }
                        if(resphp == 3){
                            Crouton.makeText(MenuPrin.this,"Ya mantenia una actividad en curso, la cual ya sobrepaso el tiempo. Ha sido terminada automaticamente.", Style.ALERT).show();
                        }
                    }catch (JSONException e) {
                        Crouton.makeText(MenuPrin.this,"Error de conexion al servidor.", Style.ALERT).show();
                    }

                }else{
                    Crouton.makeText(MenuPrin.this,"Error de conexion al servidor.", Style.ALERT).show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable throwable) {

            }

        });
    }


}
