package com.reim.chinoy.reimapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;

/**
 * Created by Bock on 27/05/2016.
 */
public class BupaEntregaEst2 extends AppCompatActivity {
    public  MediaPlayer mediaPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bupa_estrella2);

        mediaPlayer = MediaPlayer.create(this, R.raw.sonidoestrella);
        mediaPlayer.start();

    }

    public void volverMenuBE2(View v){
        mediaPlayer.release();
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/actividad2/terBupaAct2.php?idAlum=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {

                        System.gc();
                        finish();
                        Intent intent = new Intent(BupaEntregaEst2.this, ReimBupa.class);
                        startActivity(intent);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    public void bupaEntregaE2(View view){
        mediaPlayer.release();
        Usuarios usuario = (Usuarios) getApplicationContext();
        String idAlum = usuario.getIdAlumno();

        AsyncHttpClient client = new AsyncHttpClient();
        client.get("http://desarrolloreim.ulearnet.com/bupa/actividad2/terBupaAct2.php?idAlum=" + idAlum, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

                if (statusCode == 200) {
                    try {
                        System.gc();
                        finish();
                        Intent intent = new Intent(BupaEntregaEst2.this, BupaAct2.class);
                        startActivity(intent);

                    } catch (Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }
        });
    }

    //FUNCION QUE EVITAR QUE SE PUEDA RETROCEDER CON BOTON VOLVER
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (keyCode == KeyEvent.KEYCODE_BACK && event.getRepeatCount() == 0) {
            // do something on back.
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }
}
